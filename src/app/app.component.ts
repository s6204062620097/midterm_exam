import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Flight } from './flight';
import { PageService } from './share/page.service';
import { DateAdapter } from '@angular/material/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'midterm';
  flight !: Flight[];
  form !: FormGroup;
  myDate : Date;

  Airport = [{ name: 'Thailand' }, { name: 'Japan' }, { name: 'Korean' }, { name: 'United States' }];

  start!: null;

  constructor(private fb: FormBuilder, public pageService: PageService, private dateAdapter: DateAdapter<any>) {
    this.myDate = new Date();
    this.dateAdapter.setLocale('th-TH');
    this.form = this.fb.group({
      fullName: ['', Validators.required],
      from: [null, Validators.required],
      to: [null, Validators.required],
      type: ['', Validators.required],
      departure: ['', Validators.required],
      arrival: ['', Validators.required],
      adults: [0, Validators.required],
      children: [0,Validators.required,],
      infants: [0, Validators.required]
    })
  }

  ngOnInit(): void {
    this.getPage()
  }

  getPage(){
    this.flight = this.pageService.getPages()
  }

  onSubmit(f: Flight): void {
    if(f.from == f.to) return alert('จุดเริ่มต้นและจุดปลายทางต้องไม่เหมือนกัน!')
    const yearDeparture = f.departure.getFullYear() + 543
    const dateDeparture = f.departure.getDate() + "/" + (f.departure.getMonth() + 1) + "/" + yearDeparture;

    const yearArrival = f.arrival.getFullYear() + 543
    const dateArrival = f.arrival.getDate() + "/" + (f.arrival.getMonth() + 1) + "/" + yearArrival;
    f.departure = new Date((f.departure.getMonth() + 1) + '/' + f.departure.getDate() + "/" + yearDeparture)
    f.arrival = new Date((f.arrival.getMonth() + 1) + '/' + f.arrival.getDate() + "/" + yearArrival)

    this.pageService.addFlight(f)
  }
}
