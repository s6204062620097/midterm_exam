import { Injectable } from '@angular/core';
import { Flight } from '../flight';
import { MockData } from './mock-data';

@Injectable({
  providedIn: 'root',
})
export class PageService {
  flights: Flight[] = []
  constructor() {
    this.flights = MockData.mFlight
  }

  getPages(){
    return this.flights
  }

  addFlight(f:Flight){
    this.flights.push(f)
  }
}
